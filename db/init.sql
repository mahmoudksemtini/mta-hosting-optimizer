DROP DATABASE IF EXISTS ipconfig;
CREATE DATABASE ipconfig;
use ipconfig;

DROP TABLE IF EXISTS ip_config;

create table ip_config (
   id INTEGER PRIMARY KEY AUTO_INCREMENT,
   ip VARCHAR(64) NOT NULL UNIQUE,
   hostname VARCHAR(128) NOT NULL,
   active INTEGER NOT NULL,
   CHECK(active>=0 AND active <=1)
);

INSERT INTO ip_config (ip, hostname, active) VALUES('127.0.0.1','mta-prod-1', 1);
INSERT INTO ip_config (ip, hostname, active) VALUES('127.0.0.2','mta-prod-1', 0);
INSERT INTO ip_config (ip, hostname, active) VALUES('127.0.0.3','mta-prod-2', 1);
INSERT INTO ip_config (ip, hostname, active) VALUES('127.0.0.4','mta-prod-2', 1);
INSERT INTO ip_config (ip, hostname, active) VALUES('127.0.0.5','mta-prod-2', 0);
INSERT INTO ip_config (ip, hostname, active) VALUES('127.0.0.6','mta-prod-3', 0);
