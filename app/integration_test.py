#!/usr/local/bin/ python

import unittest
import os
import requests

class IntegrationTestsMtaHostingOptimizer(unittest.TestCase):

    def test_app_available(self):
        """ Is the app available on localhost:5000 """
        r = requests.get('http://localhost:5000')
        self.assertTrue(r.status_code == 200)

    def test_expected_output_api(self):
        """ Is the API returning the expected output """
        cmd = 'docker exec mta-hosting-optimizer_app_1 bash -c \'echo "$MIN_ACTIVE"\''
        stream = os.popen(cmd)
        min_active = stream.read()
        min_active = str(min_active.strip())
        if min_active == '0':
            expected = '[]'
        elif min_active == '1':
            expected = '["mta-prod-1", "mta-prod-3"]'
        else:
            expected = '["mta-prod-1", "mta-prod-2", "mta-prod-3"]'
        r = requests.get('http://localhost:5000')
        self.assertTrue(r.text == expected)

if __name__ == '__main__':
    unittest.main()
