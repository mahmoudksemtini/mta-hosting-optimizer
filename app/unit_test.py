#!/usr/local/bin/ python

import unittest
import os
import app

class UnitTestsMtaHostingOptimizer(unittest.TestCase):
    def get_expected_output(self):
        if os.environ['MIN_ACTIVE'] == '0':
            expected = '[]'
        elif os.environ['MIN_ACTIVE'] == '1':
            expected = "['mta-prod-1', 'mta-prod-3']"
        else:
            expected = "['mta-prod-1', 'mta-prod-2', 'mta-prod-3']"
        return expected

    def test_expected_output_app(self):
        """ App function returning the expected output"""
        expected = self.get_expected_output()
        returned_resp = str(app.mta_hosting_optimizer())
        self.assertTrue(returned_resp == expected)

if __name__ == '__main__':
    unittest.main()
