from typing import List, Dict
from flask import Flask
import mysql.connector
import json
import os

app = Flask(__name__)

MIN_ACTIVE = os.environ['MIN_ACTIVE']

def mta_hosting_optimizer() -> List[Dict]:
    config = {
        'user': 'root',
        'password': 'root',
        'host': 'db',
        'port': '3306',
        'database': 'ipconfig'
    }
    connection = mysql.connector.connect(**config)
    cursor = connection.cursor()
    query = " select hostname from ip_config where active=1 group by hostname having count(active) <=" + str(MIN_ACTIVE)
    query += " union select hostname from ip_config group by hostname having count(active) <= " + str(MIN_ACTIVE)
    cursor.execute(query)
    results = [list(hostname)[0] for hostname in cursor]
    cursor.close()
    connection.close()
    return results


@app.route('/')
def index() -> str:
    return json.dumps(mta_hosting_optimizer())

if __name__ == '__main__':
    app.run(host='0.0.0.0')
