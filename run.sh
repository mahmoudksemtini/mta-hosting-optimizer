#!/bin/bash
set -e

usage(){
echo "Usage: $0 [-h] [-m, --min-active MIN_ACTIVE]"
echo "-h, --help         show this help message and exit"
echo "-m , --min-active  MIN_ACTIVE"
exit 1
}

MIN_ACTIVE=1

while [ "$1" != "" ]; do
    case $1 in
        -m | --min-active )
                                shift
                                MIN_ACTIVE=$1
                                shift
                                ;;
        -h | --help )
                                usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
done



export MIN_ACTIVE=$MIN_ACTIVE
docker-compose up -d --build --force-recreate
