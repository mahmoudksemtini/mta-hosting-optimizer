#!/usr/bin/env bash
set -e

if [ ! "$(docker ps -q -f name=mta-hosting-optimizer_app_1)" ] ;
  then echo 'Please run run.sh first';
  exit 1;
fi
printf "\033[0;32mRunning integration tests ...\033[0m \n"
python3 app/integration_test.py
printf "\033[0;32mRunning unit tests ...\033[0m \n"
docker exec -i  mta-hosting-optimizer_app_1 sh -c 'cd /app/ && python3 unit_test.py'