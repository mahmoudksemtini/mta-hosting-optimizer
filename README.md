# mta-hosting-optimizer
A service that uncovers the inefficient servers hosting only few active MTAs.

## Prerequisites
1. Docker CE:
* [Linux](https://docs.docker.com/install/linux/docker-ce/ubuntu/)
* [MacOS](https://docs.docker.com/docker-for-mac/install/)
* [Windows](https://docs.docker.com/docker-for-windows/install/)

2. [Docker-compose](https://docs.docker.com/compose/install/)

## Project structure:
```
mta-hosting-optimizer
├── README.md
├── app                         # App source directory
│   ├── Dockerfile
│   ├── app.py                  # Main service
│   ├── integration_test.py     # Integrartion ests for the application
│   ├── requirements.txt
│   └── unit_test.py            # Unit tests for app.py
├── db                          # DB source directory (for mock data)
│   └── init.sql                # script to initiate the database
├── docker-compose.yml          # to deploy the app locally
├── run.sh                      # wrapper to launch the app & db
└── test.sh                     # script to test the project
```


## How does it work:

* To start the project, simply run:
```
./run.sh -m <minimum_active_addresses>
```
If _-m_ argument is not provided, the default value _1_ will be used. 

* Calling the endpoints:
```
curl http://localhost:5000
```
This should return a set of hosts having less or equals <minimum_active_addresses> active IP addresses. 

## Testing:
To start the tests, run:
```
./test.sh
```
